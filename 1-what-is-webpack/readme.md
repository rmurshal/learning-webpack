# What is Webpack

Webpack adalah sebuah bundler module, yang mengambil semua sumber file project (javascript, css, image, font) dan menggabungkannya menjadi satu atau beberapa file yang lebih kecil (yang disebut bundle).

Tujuan dari webpack sendiri adalah untuk meningkatkan performa loading dari aplikasi serta melakukan transpilasi kode javascript modern ke versi yang lebih lama sehingga dapat dijalankan di semua browser.
