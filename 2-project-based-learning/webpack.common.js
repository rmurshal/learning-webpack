let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        // put key-value here
        main: './src/index.js',
        vendor: './src/vendor.js',
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader', // 3. insert common js into the dom
                    'css-loader', // 2. Convert css into common js
                    'sass-loader', // 1. Convert scss/sass into css
                ],
            },
            {
                test: /\.html$/,
                use: ['html-loader'],
            },
            {
                test: /\.(svg|png|jpg|gif)$/,
                type: 'asset/resource', // tell the webpack to treat the file as resources and separate them (config in output-assetModuleFilename property)
                // if we use "asset/inline", webpack will merge them in js bundle, and maybe no need to use assetModuleFilename property in output
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/template.html',
        }),
    ],
}
