# Some Notes About Webpack

1. Default command: `webpack`

2. Default entry point: `./src/index.js`

3. Default bundle path: `./dist/main.js`

4. Command untuk menambahkan konfigurasi webpack secara manual: `webpack --config <path_to_config_file>`

5. Property `mode` pada configuration, jika value `development` maka webpack belum akan melakukan minifying code. Jika value `production` maka webpack akan melakukan minifying code. Default value `production`.

6. Loaders adalah module yang mengatasi berbagai jenis file seperti CSS/SASS, Gambar, Font, dan lainnya lalu kemudian melakukan optimasi terhadap file-file tersebut.

7. Module `css-loader` melihat seluruh file css yang ada lalu mengkonversinya ke dalam sebuah file javascript. (Loaders)

8. Module `style-loader` melakukan injeksi css tersebut ke dalam DOM sehingga rule css dalam diaplikasikan pada HTML. (Loaders)

9. Simple regex: `/\.css$/`, diawali dan diakhiri dengan `//`, untuk menambahkan karakter menggunakan `\` seperti `\.`, dan `$` berarti harus berakhir dengan `.css`.

10. Module `sass-loader` mengkonversi file scss yang dibaca menjadi file css. Module ini membutuhkan module `node-sass` untuk dapat bekerja. (Loaders)

11. Konsep `contentHash` yaitu memberikan string unik sebagai nama saat men-generate file. String unik ini dihasilkan dari hasil hashing isi file tersebut. String unik ini nanti yang akan menentukan apakah browser akan melakukan request / mengambil resources kembali dari server atau menggunakan file cache yang disimpan pada browser. Jika file name sama, maka akan menggunakan file cache, jika berbeda maka akan menggunakan file yang dikirimkan dari server.

12. Plugins pada webpack digunakan untuk memperluas fungsionalitas dari webpack sesuai kebutuhan pengguna.

13. Notes kembali bahwa `loaders` dan `plugins` pada webpack adalah 2 hal yang berbeda. Loaders umumnya digunakan untuk memproses file sumber (contoh css-loader), sedangkan plugins digunakan untuk memperluas fungsionalitas webpack (contoh html-webpack-plugin).

14. Module `html-webpack-plugin` digunakan untuk menghasilkan file HTML. Contoh plugins lain yang umum digunakan yaitu `UglifyJsPlugin` untuk optimasi file JavaScript. (Plugins)
